/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/>
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.runelite.client.plugins.castlewars;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.util.Map;
import javax.inject.Inject;

import net.runelite.api.*;
import net.runelite.api.coords.LocalPoint;
import net.runelite.api.coords.WorldPoint;
import net.runelite.api.widgets.Widget;
import net.runelite.client.plugins.castlewars.id.NpcID;
import net.runelite.client.plugins.castlewars.id.ObjectID;
import net.runelite.client.ui.overlay.Overlay;
import net.runelite.client.ui.overlay.OverlayLayer;
import net.runelite.client.ui.overlay.OverlayPosition;
import net.runelite.client.ui.overlay.OverlayPriority;

/**
 * Scene overlay for NPCs and GameObjects ->
 * Barricades,
 * Rocks.
 */
public class SceneOverlay extends Overlay 
{

	private final Client client;
	private final CastleWarsConfig config;
	private final CastleWarsPlugin plugin;

	@Inject
	SceneOverlay(Client client, CastleWarsConfig config, CastleWarsPlugin plugin)
	{
		this.client = client;
        this.config = config;
		this.plugin = plugin;
		setLayer(OverlayLayer.ABOVE_SCENE);
		setPosition(OverlayPosition.DYNAMIC);
		setPriority(OverlayPriority.HIGH);
	}

	@Override
	public Dimension render(Graphics2D graphics)
	{
		LocalPoint playerLocation = client.getLocalPlayer().getLocalLocation();

       /**
		* NPCs to highlight
        */
		for (NPC npc : plugin.getHighlightBarricades())
		{
                     switch (npc.getId())
					 {
                          case NpcID.SARADOMIN_BARRICADE: // Saradomin Barricade
                          case NpcID.SARADOMIN_BARRICADE_LIT: // Saradomin Barricade Lit
                          renderNpcSceneOverlay(graphics, npc, config.getSaradominHighlightColor());
                          break;
                          case NpcID.ZAMORAK_BARRICADE: // Zamorak Barricade
                          case NpcID.ZAMORAK_BARRICADE_LIT: // Zamorak Barricade Lit
                          renderNpcSceneOverlay(graphics, npc, config.getZamorakHighlightColor());
                          break;
					 }
		}

        /**
         * GameObjects to highlight
         */
        for (GameObject gameObject : plugin.getHighlightRocks())
		{
                     switch (gameObject.getId())
					 {
                         case ObjectID.ROCKS_FULL: // Underground rocks full
                         case ObjectID.ROCKS_HALF: // Underground rocks half
                         renderGameObjectSceneOverlay(graphics, gameObject, config.getRocksHighlightColor());
                         break;
					 }
		}

		/**
		 * WallObjects to highlight
		 */
		Map<WorldPoint, WallObject> doors = plugin.getHighlightDoors();

		if (doors.isEmpty())
		{
			return null;
		}

		final Widget viewport = client.getViewportWidget();

		for (final WallObject door : doors.values())
		{
			if (viewport == null)
			{
				continue;
			}

			switch (door.getId())
			{
				/**
				 * Open Doors
				 */
				case ObjectID.SARADOMIN_SMALL_DOOR_OPEN:
				case ObjectID.SARADOMIN_BIG_DOOR_OPEN_LEFT:
				case ObjectID.SARADOMIN_BIG_DOOR_OPEN_RIGHT:

				case ObjectID.ZAMORAK_SMALL_DOOR_OPEN:
				case ObjectID.ZAMORAK_BIG_DOOR_OPEN_LEFT:
				case ObjectID.ZAMORAK_BIG_DOOR_OPEN_RIGHT:
				renderWallObjectSceneOverlay(graphics, door, config.getOpenDoorsHighlightColor());
				break;

				/**
				 * Closed Doors
				 */
				case ObjectID.SARADOMIN_SMALL_DOOR_CLOSED:
				case ObjectID.SARADOMIN_BIG_DOOR_CLOSED_LEFT:
				case ObjectID.SARADOMIN_BIG_DOOR_CLOSED_RIGHT:

				case ObjectID.ZAMORAK_SMALL_DOOR_CLOSED:
				case ObjectID.ZAMORAK_BIG_DOOR_CLOSED_LEFT:
				case ObjectID.ZAMORAK_BIG_DOOR_CLOSED_RIGHT:
				renderWallObjectSceneOverlay(graphics, door, config.getClosedDoorsHighlightColor());
				break;
			}
		}
		return null;
	}

	/**
         * Render NPC Highlights
         * @param graphics
         * @param actor
         * @param color 
         */
	private void renderNpcSceneOverlay(Graphics2D graphics, NPC actor, Color color)
	{
		NPCDefinition npcDefinition = actor.getTransformedDefinition();
		if (npcDefinition == null || !npcDefinition.isClickable())
		{
			return;
		}

		Point mousePosition = client.getMouseCanvasPosition();
		Shape objectClickbox = actor.getConvexHull();
		if (objectClickbox == null)
		{
			return;
		}

		if (objectClickbox.contains(mousePosition.getX(), mousePosition.getY()))
		{
			renderPoly(graphics, color.darker(), objectClickbox);
		}
		else
		{
			renderPoly(graphics, color, objectClickbox);
		}

	}

        /**
         * Render GameObjects Highlights
         * @param graphics
         * @param actor
         * @param color 
         */
        private void renderGameObjectSceneOverlay(Graphics2D graphics, GameObject actor, Color color)
	{
		GameObject gameObject = actor;
		final ObjectDefinition objectDefinition = client.getObjectDefinition(gameObject.getId());
		if (gameObject == null || objectDefinition == null)
		{
			return;
		}

		Point mousePosition = client.getMouseCanvasPosition();
		Shape objectClickbox = actor.getConvexHull();
		if (objectClickbox == null)
		{
			return;
		}

		if (objectClickbox.contains(mousePosition.getX(), mousePosition.getY()))
		{
			renderPoly(graphics, color.darker(), objectClickbox);
		}
		else
		{
			renderPoly(graphics, color, objectClickbox);
		}

	}

	/**
	 * Render WallObjects Highlights for doors
	 * @param graphics
	 * @param actor
	 * @param color
	 */
	private void renderWallObjectSceneOverlay(Graphics2D graphics, WallObject actor, Color color)
	{
		WallObject wallObject = actor;
		final ObjectDefinition objectDefinition = client.getObjectDefinition(wallObject.getId());
		if (objectDefinition == null || wallObject.getWorldLocation().distanceTo(client.getLocalPlayer().getWorldLocation()) >= 9
				|| wallObject == null
				|| wallObject.getClickbox() == null
				|| wallObject.getWorldLocation().getPlane() != client.getPlane()
				|| wallObject.getLocalLocation().distanceTo(client.getLocalPlayer().getLocalLocation()) >= 2030)
		{
			return;
		}

		Point mousePosition = client.getMouseCanvasPosition();
		Shape objectClickbox = actor.getClickbox();
		if (objectClickbox == null)
		{
			return;
		}

		if (objectClickbox.contains(mousePosition.getX(), mousePosition.getY()))
		{
			renderPoly(graphics, color.darker(), objectClickbox);
		}
		else
		{
			renderPoly(graphics, color, objectClickbox);
		}

	}
        
         /**
          * Render Polygon
          * @param graphics
          * @param color
          * @param polygon 
          */
	private void renderPoly(Graphics2D graphics, Color color, Shape polygon)
	{
		if (polygon != null)
		{
			graphics.setColor(color);
			graphics.setStroke(new BasicStroke(2));
			graphics.draw(polygon);
			graphics.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), 20));
			graphics.fill(polygon);
		}
	}
}

