/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/>
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.runelite.client.plugins.castlewars;

import com.google.inject.Provides;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.runelite.api.*;
import net.runelite.api.coords.LocalPoint;
import net.runelite.api.coords.WorldPoint;
import net.runelite.api.events.*;
import net.runelite.api.util.Text;
import net.runelite.client.config.ConfigManager;
import net.runelite.client.eventbus.Subscribe;
import net.runelite.client.events.ConfigChanged;
import net.runelite.client.plugins.Plugin;
import net.runelite.client.plugins.PluginDescriptor;
import net.runelite.client.plugins.PluginType;
import net.runelite.client.plugins.castlewars.id.*;
import net.runelite.client.plugins.castlewars.id.NpcID;
import net.runelite.client.plugins.castlewars.id.ObjectID;
import net.runelite.client.plugins.castlewars.wrappers.Barricade;
import net.runelite.client.plugins.castlewars.wrappers.Door;
import net.runelite.client.plugins.castlewars.wrappers.Rock;
import net.runelite.client.plugins.castlewars.wrappers.Tap;
import net.runelite.client.ui.overlay.OverlayManager;
import org.pf4j.Extension;

@Extension
@PluginDescriptor(
	name = "Castle Wars",
	description = "Castle Wars Plugin",
	tags = {"castlewars", "minigame"},
    type = PluginType.MINIGAME
)

@Slf4j
public class  CastleWarsPlugin extends Plugin
{

    /**
     * The client
     */
    @Inject
    private Client client;

    /**
     * The config
     */
    @Inject
    private CastleWarsConfig config;

    /**
     * The overlay manager
     */
    @Inject
    private OverlayManager overlayManager;

    /**
     * Scene overlay for NPCs and GameObjects (barricades/rocks)
     */
    @Inject
    private SceneOverlay SceneOverlay;

    /**
     * MiniMap overlay for NPCs(barricades), GameObjects(rocks) and for open tunnels underground
     */
    @Inject
    private MinimapOverlay MinimapOverlay;

    /**
     * TindTimer overlay for lit barricades
     */
    @Inject
    private TindTimerOverlay TindTimerOverlay;

    /**
     * Castle Wars game time overlay for resized modes
     */
    @Inject
    private GameTimeOverlay GameTimeOverlay;

    /**
     * Saradomin standard(flag)
     */
    @Getter(AccessLevel.PACKAGE)
    private GameObject saradominStandard;

    /**
     * Zamorak standard(flag)
     */
    @Getter(AccessLevel.PACKAGE)
    private GameObject zamorakStandard;

    /**
     * DeSpawned rocks underground
     */
    @Getter(AccessLevel.PACKAGE)
    private final List<WorldPoint> deSpawnedRocks = new ArrayList<>();

    /**
     * SpawnedRocks underground to highlight
     */
    @Getter(AccessLevel.PACKAGE)
    private final Map<WorldPoint, Rock> highlightRocks = new HashMap<>();

    /**
     * Set TindTimer on lit barricades
     */
    @Getter(AccessLevel.PACKAGE)
    private final Map<WorldPoint, Barricade> litBarricades = new HashMap<>();

    /**
     * Last action time for TindTimer
     */
    @Getter(AccessLevel.PACKAGE)
    private Instant lastActionTime = Instant.ofEpochMilli(0);

    /**
     * The barricades to highlight for each team blue/red
     */
    @Getter(AccessLevel.PACKAGE)
    private final Map<WorldPoint, Barricade> highlightBarricades = new HashMap<>();

    /**
     * The doors to highlight
     */
    @Getter(AccessLevel.PACKAGE)
    private final Map<WorldPoint, Door> highlightDoors = new HashMap<>();

    /**
     * The taps to highlight
     */
    @Getter(AccessLevel.PACKAGE)
    private final Map<WorldPoint, Tap> highlightTaps = new HashMap<>();

    /**
     * @param configManager
     * @return CastleWarsConfig
     */
    @Provides
    CastleWarsConfig getConfig(ConfigManager configManager)
    {
        return configManager.getConfig(CastleWarsConfig.class);
    }

    @SneakyThrows
    @Subscribe
    public void onConfigChanged(ConfigChanged event)
    {
        if (!event.getGroup().equals("castlewars"))
        {
            return;
        }

        if (event.getKey().equals("mirrorMode"))
        {
            SceneOverlay.determineLayer();
            MinimapOverlay.determineLayer();
            TindTimerOverlay.determineLayer();
            GameTimeOverlay.determineLayer();
            overlayManager.remove(SceneOverlay);
            overlayManager.remove(MinimapOverlay);
            overlayManager.remove(TindTimerOverlay);
            overlayManager.remove(GameTimeOverlay);
            overlayManager.add(SceneOverlay);
            overlayManager.add(MinimapOverlay);
            overlayManager.add(TindTimerOverlay);
            overlayManager.add(GameTimeOverlay);

        }

        if (!config.rocksHighlight())
        {
            highlightRocks.clear();
        }
        if (!config.useTindTimer())
        {
            litBarricades.clear();
        }
        if (!config.tapHighlight())
        {
            highlightTaps.clear();
        }
        rebuildAllHighlightBarricades();
        TindTimerOverlay.updateConfig();
    }

    /**
     * Rebuild highlight barricades when config has changed.
     */
    private void rebuildAllHighlightBarricades()
    {
        highlightBarricades.clear();

        if (client.getGameState() != GameState.LOGGED_IN &&
                client.getGameState() != GameState.LOADING)
        {
            return;
        }

        for (NPC npc : client.getNpcs())
        {
            final WorldPoint npcLocation = npc.getWorldLocation();
            switch (npc.getId())
            {
                case NpcID.SARADOMIN_BARRICADE:
                case NpcID.ZAMORAK_BARRICADE:
                    if (config.barricadeHighlight())
                    {
                        highlightBarricades.put(npcLocation, new Barricade(npc));
                    }
                    break;

            }
        }
    }

    @SneakyThrows
    @Override
    protected void startUp()
    {
        overlayManager.add(SceneOverlay);
        overlayManager.add(MinimapOverlay);
        overlayManager.add(TindTimerOverlay);
        overlayManager.add(GameTimeOverlay);
        TindTimerOverlay.updateConfig();
    }

    @SneakyThrows
    @Override
    protected void shutDown()
    {
        overlayManager.remove(SceneOverlay);
        overlayManager.remove(MinimapOverlay);
        overlayManager.remove(TindTimerOverlay);
        overlayManager.remove(GameTimeOverlay);
        lastActionTime = Instant.ofEpochMilli(0);
        litBarricades.clear();
        highlightBarricades.clear();
        highlightRocks.clear();
        deSpawnedRocks.clear();
    }

    @Subscribe
    public void onGameStateChanged(final GameStateChanged event)
    {
        /**
         * Clear all things when logged out.
         */
        if (event.getGameState() == GameState.HOPPING || event.getGameState() == GameState.LOGIN_SCREEN)
        {
            deSpawnedRocks.clear();
            litBarricades.clear();
            highlightBarricades.clear();
            highlightRocks.clear();
            saradominStandard = null;
            zamorakStandard = null;
        }
    }

    /**
     * Method to check if inside Castle Wars.
     */
    private boolean inCastleWars()
    {
        Player localPlayer = client.getLocalPlayer();
        return localPlayer != null && localPlayer.getWorldLocation().getRegionID() >= RegionID.CASTLE_WARS  // 9520 = Castle Wars
                && localPlayer.getWorldLocation().getRegionID() <= RegionID.CASTLE_WARS_UNDERGROUND; // 9620 = Castle Wars underground
    }

    @Subscribe
    public void onMenuEntryAdded(MenuEntryAdded menuEntryAdded)
    {
        String option = Text.removeTags(menuEntryAdded.getOption()).toLowerCase();
        String target = Text.standardize(menuEntryAdded.getTarget());
        int identifier = menuEntryAdded.getIdentifier();
        MenuEntry[] menuEntries = client.getMenuEntries();
        Player[] players = client.getCachedPlayers();
        NPC[] npcs = client.getCachedNPCs();

        /**
         * Don't hide player options when not inside Castle Wars, or when using bandages to heal other players.
         */
        if (!inCastleWars() || target.contains("bandages"))
            return;

        /**
         * Hide player options when using an item on a player.
         */
        if (config.hidePlayerOptions() && option.startsWith("use"))
        {
             Player player = null;

            if (identifier >= 0 && identifier < players.length)
                player = players[identifier];

            if (player == null)
                return;

            if (menuEntries.length > 0 && target.contains(player.getName().toLowerCase()))
                client.setMenuEntries(Arrays.copyOf(menuEntries, menuEntries.length - 1));
        }

        /**
         * Hide barricade(NPC) options when standard(flag, GameObject) is on same location as the barricade(NPC).
         */
        if (config.hideNpcOptions())
        {
            if (saradominStandard == null && zamorakStandard == null)
                return;

            NPC npc = null;

            if (identifier >= 0 && identifier < npcs.length)
                npc = npcs[identifier];

           if (npc == null)
               return;

            if (saradominStandard != null && saradominStandard.getLocalLocation().equals(npc.getLocalLocation()) || zamorakStandard != null && zamorakStandard.getLocalLocation().equals(npc.getLocalLocation()))
                if (menuEntries.length > 0 && target.contains(npc.getName().toLowerCase()))
                    client.setMenuEntries(Arrays.copyOf(menuEntries, menuEntries.length -1));
        }
    }

    @Subscribe
    public void onGameObjectSpawned(GameObjectSpawned event)
    {
        final GameObject gameObject = event.getGameObject();
        final Tile tile = event.getTile();
        WorldPoint location = tile.getWorldLocation();
        final WorldPoint gameObjectLocation = gameObject.getWorldLocation();

        switch (gameObject.getId())
        {
            /**
             * This is used to know which standard(flag) is dropped by a player.
             */
            case ObjectID.SARADOMIN_STANDARD: // Saradomin Standard
                this.saradominStandard = gameObject;
                log.debug("Saradomin flag spawn: {}", gameObject);
                break;

            case ObjectID.ZAMORAK_STANDARD: // Zamorak Standard
                this.zamorakStandard = gameObject;
                log.debug("Zamorak flag spawn: {}", gameObject);
                break;

            /**
             * Remove tunnel MiniMap overlay if rock has spawned and add highlights on rocks.
             */
            case ObjectID.ROCKS_FULL: // Underground rocks full
            case ObjectID.ROCKS_HALF: // Underground rocks half
                deSpawnedRocks.remove(location);
                if (config.rocksHighlight() && gameObject.getWorldLocation().getRegionID() == RegionID.CASTLE_WARS_UNDERGROUND)
                {
                    highlightRocks.put(gameObjectLocation, new Rock(gameObject));
                }
                log.debug("Rock spawn: {}", gameObject);
                break;
        }
    }

    @Subscribe
    public void onGameObjectDespawned(GameObjectDespawned event)
    {
        final GameObject gameObject = event.getGameObject();
        final Tile tile = event.getTile();
        WorldPoint location = tile.getWorldLocation();
        final WorldPoint gameObjectLocation = gameObject.getWorldLocation();
        switch (gameObject.getId())
        {
            /**
             * null standards if it has been taken again.
             */
            case ObjectID.SARADOMIN_STANDARD: // Saradomin Standard
                this.saradominStandard = null;
                break;

            case ObjectID.ZAMORAK_STANDARD: // Zamorak Standard
                this.zamorakStandard = null;
                break;

            /**
             * Remove highlight and add tunnel MiniMap overlay if rock has been deSpawned.
             */
            case ObjectID.ROCKS_FULL: // Underground rocks Full
            case ObjectID.ROCKS_HALF: // Underground rocks half
                highlightRocks.remove(gameObjectLocation);
                deSpawnedRocks.add(location);
                break;
        }
    }

    @Subscribe
    public void onGameTick(GameTick tick)
    {
        /**
         * Set hintArrow on player with the standard(flag).
         */
        for (Player player : client.getPlayers())
        {
            int[] equipmentIds = player.getPlayerAppearance().getEquipmentIds();
            for (int equipmentId : equipmentIds)
            {
                if (equipmentId == EquipmentID.SARADOMIN_STANDARD || equipmentId == EquipmentID.ZAMORAK_STANDARD)
                    client.setHintArrow(player);
            }
        }

        /**
         * Clear highlights from rocks if player is not underground.
         */
        if (client.getLocalPlayer().getWorldLocation().getRegionID() != RegionID.CASTLE_WARS_UNDERGROUND && !highlightRocks.isEmpty())
        {
            highlightRocks.clear();
        }

        /**
         * Clear highlights from doors if not inside CastleWars or when underground.
         */
          if (!inCastleWars()
                  && !highlightDoors.isEmpty()
                  || !highlightDoors.isEmpty() && client.getLocalPlayer().getWorldLocation().getRegionID() == RegionID.CASTLE_WARS_UNDERGROUND)
          {
              highlightDoors.clear();
          }

        /**
         * Check if all TindTimers are still there, and remove the ones that are not.
         */
        Iterator<Map.Entry<WorldPoint, Barricade>> it = litBarricades.entrySet().iterator();
        Tile[][][] tiles = client.getScene().getTiles();

        //Instant expire = Instant.now().minus(Barricade.TIND_TIME.multipliedBy(2));
        Instant expire = Instant.now().plusSeconds(7);

        while (it.hasNext())
        {
            Map.Entry<WorldPoint, Barricade> entry = it.next();
            Barricade cade = entry.getValue();
            WorldPoint world = entry.getKey();
            LocalPoint local = LocalPoint.fromWorld(client, world);

            // Not within the client's viewport
            if (local == null)
            {
                // remove TindTimer if it has expired
                if (cade.getLitOn().isBefore(expire))
                {
                    it.remove();
                }
            }
        }
    }

    @Subscribe
    public void onNpcSpawned(NpcSpawned npcSpawned)
    {
        /**
         * Only allow this to work when barricadeHighlight is enabled in config.
         */
        if (!config.barricadeHighlight())
            return;

        NPC npc = npcSpawned.getNpc();
        final WorldPoint npcLocation = npc.getWorldLocation();

        switch (npc.getId())
        {
            /**
             * Set highlights on barricades. ->
             * Normal barricade,
             * Lit barricade
             */
            case NpcID.SARADOMIN_BARRICADE:
            case NpcID.SARADOMIN_BARRICADE_LIT:
                highlightBarricades.put(npcLocation, new Barricade(npc));
                log.debug("Saradomin barricade spawn: {}", npc);
                break;

            case NpcID.ZAMORAK_BARRICADE:
            case NpcID.ZAMORAK_BARRICADE_LIT:
                highlightBarricades.put(npcLocation, new Barricade(npc));
                log.debug("Zamorak barricade spawn: {}", npc);
                break;
        }}

    @Subscribe
    public void onNpcDefinitionChanged(NpcDefinitionChanged npcDefinitionChanged)
    {
        NPC npc = npcDefinitionChanged.getNpc();
        final WorldPoint npcLocation = npc.getWorldLocation();

        switch (npc.getId())
        {
            /**
             * Remove TindTimers from lit barricades when bucket is used.
             */
            case NpcID.SARADOMIN_BARRICADE_LIT:
            case NpcID.ZAMORAK_BARRICADE_LIT:
                litBarricades.remove(npcLocation);
                break;

            /**
             * Set highlights and TindTimers on lit barricades.
             */
            case NpcID.SARADOMIN_BARRICADE:
            case NpcID.ZAMORAK_BARRICADE:
                if (config.useTindTimer())
                {
                    litBarricades.put(npcLocation, new Barricade(npc));
                    lastActionTime = Instant.now();
                }
              break;
        }}

   @Subscribe
	public void onNpcDespawned(NpcDespawned npcDespawned)
	{
        /**
         * Remove highlights and TindTimers from lit barricades.
         */
            NPC npc = npcDespawned.getNpc();
            final WorldPoint npcLocation = npc.getWorldLocation();
            //final Barricade litcade = barricades.get(npcLocation);

            switch (npc.getId())
            {
                           case NpcID.SARADOMIN_BARRICADE:
                           case NpcID.ZAMORAK_BARRICADE:
                           highlightBarricades.remove(npcLocation);
                           break;

                           case NpcID.SARADOMIN_BARRICADE_LIT:
                           case NpcID.ZAMORAK_BARRICADE_LIT:
                           highlightBarricades.remove(npcLocation);
                           litBarricades.remove(npcLocation);
                           break;
        }}

    @Subscribe
    public void onWallObjectSpawned(WallObjectSpawned event)
    {
        WallObject wallObject = event.getWallObject();
        final ObjectDefinition comp = client.getObjectDefinition(wallObject.getId());
        final ObjectDefinition impostor = comp.getImpostorIds() != null ? comp.getImpostor() : comp;
        final WorldPoint wallObjectLocation = wallObject.getWorldLocation();

        if (wallObject != null
                && ObjectID.wallObject_Ids_DOORS.contains(wallObject.getId())
                && impostor != null
                && impostor.getId() == wallObject.getId()
                && ObjectID.wallObject_Ids_DOORS.contains(impostor.getId()))
        {
            highlightDoors.put(wallObjectLocation, new Door(wallObject));
        }
    }

    @Subscribe
    public void onWallObjectDespawned(WallObjectDespawned event)
    {
        WallObject wallObject = event.getWallObject();
        final WorldPoint wallObjectLocation = wallObject.getWorldLocation();

        if (ObjectID.wallObject_Ids_DOORS.contains(wallObject.getId()))
        {
            highlightDoors.remove(wallObjectLocation);
        }
    }

    @Subscribe
    public void onDecorativeObjectSpawned(DecorativeObjectSpawned event)
    {
        DecorativeObject decorativeObject = event.getDecorativeObject();
        final WorldPoint decorativeObjectLocation = decorativeObject.getWorldLocation();

        switch (decorativeObject.getId())
        {
            case ObjectID.TAP:
                if (decorativeObject.getWorldLocation().equals(new WorldPoint(2431, 3077, 0))
                || decorativeObject.getWorldLocation().equals(new WorldPoint(2368, 3130, 0)))
                {
                    highlightTaps.put(decorativeObjectLocation, new Tap(decorativeObject));
                }
            break;
        }
    }

    @Subscribe
    public void onDecorativeObjectDespawned(DecorativeObjectDespawned event)
    {
        DecorativeObject decorativeObject = event.getDecorativeObject();
        final WorldPoint decorativeObjectLocation = decorativeObject.getWorldLocation();

        switch (decorativeObject.getId())
        {
            case ObjectID.TAP:
                highlightTaps.remove(decorativeObjectLocation);
                break;
        }
    }

  /*  @Subscribe
    public void onItemSpawned(ItemSpawned event)
    {
        TileItem item = event.getItem();
        Tile tile = item.getTile();
        final WorldPoint itemLocation = item.getTile().getWorldLocation();
        switch (item.getId())
        {
            case GroundItemID.TINDERBOX:
            case GroundItemID.BUCKET:
                highlightGroundItems.put(itemLocation, item);
            break;
        }
    }

    @Subscribe
    public void onItemDespawned(ItemDespawned event)
    {
        TileItem item = event.getItem();
        Tile tile = item.getTile();
        switch (item.getId())
        {
            case GroundItemID.TINDERBOX:
            case GroundItemID.BUCKET:
                highlightGroundItems.remove(item.getTile().getWorldLocation());
                break;
        }
    }*/
}
