package net.runelite.client.plugins.castlewars.wrappers;

import lombok.Getter;
import net.runelite.api.GameObject;
import net.runelite.api.coords.WorldPoint;

/**
 * Wrapper class for GameObjects that represents a rock.
 */
public class Rock
{
    /**
     * The GameObject rock
     */
    @Getter
    private GameObject rock;

    /**
     * The rock id
     */
    @Getter
    private int rockID;

    /**
     *  The worldLocation
     */
    @Getter
    private WorldPoint worldLocation;

    /**
     * Constructor for a rock GameObject
     * @param rock
     */
    public Rock(GameObject rock)
    {
        this.rock = rock;
        this.rockID = rock.getId();
        this.worldLocation = rock.getWorldLocation();
    }

}
