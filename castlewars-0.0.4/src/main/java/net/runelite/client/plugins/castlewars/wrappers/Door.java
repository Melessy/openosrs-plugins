package net.runelite.client.plugins.castlewars.wrappers;

import lombok.Getter;
import net.runelite.api.WallObject;
import net.runelite.api.coords.WorldPoint;

/**
 * Wrapper class for WallObjects that represents a door.
 */
public class Door
{
    /**
     * The WallObject door
     */
    @Getter
    private WallObject door;

    /**
     * The id
     */
    @Getter
    private int doorID;

    /**
     *  The worldLocation
     */
    @Getter
    private WorldPoint worldLocation;

    /**
     * Constructor for a door WallObject
     * @param door
     */
    public Door(WallObject door)
    {
        this.door = door;
        this.doorID = door.getId();
        this.worldLocation = door.getWorldLocation();
    }

}

