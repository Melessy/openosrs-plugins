package net.runelite.client.plugins.castlewars.wrappers;

import lombok.Getter;
import net.runelite.api.DecorativeObject;
import net.runelite.api.coords.WorldPoint;

/**
 * Wrapper class for a DecorativeObjects that represents a tap.
 */
public class Tap
{
    /**
     * The DecorativeObject Tap
     */
    @Getter
    private DecorativeObject tap;

    /**
     * The tap id
     */
    @Getter
    private int tapID;

    /**
     *  The worldLocation
     */
    @Getter
    private WorldPoint worldLocation;

    /**
     * Constructor for a Tap DecorativeObject
     * @param tap
     */
    public Tap(DecorativeObject tap)
    {
        this.tap = tap;
        this.tapID = tap.getId();
        this.worldLocation = tap.getWorldLocation();
    }

}

