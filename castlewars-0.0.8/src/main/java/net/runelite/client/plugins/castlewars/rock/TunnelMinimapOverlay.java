/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/>
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.runelite.client.plugins.castlewars.rock;

import net.runelite.api.*;
import net.runelite.api.coords.LocalPoint;
import net.runelite.api.coords.WorldPoint;
import net.runelite.client.plugins.castlewars.CastleWarsConfig;
import net.runelite.client.plugins.castlewars.CastleWarsPlugin;
import net.runelite.client.plugins.castlewars.id.RegionID;
import net.runelite.client.ui.overlay.*;

import javax.inject.Inject;
import java.awt.*;

public class TunnelMinimapOverlay extends Overlay
{
    private final Client client;
    private final CastleWarsConfig config;
    private final CastleWarsPlugin plugin;

    @Inject
    TunnelMinimapOverlay(Client client, CastleWarsConfig config, CastleWarsPlugin plugin)
    {
        this.client = client;
        this.config = config;
        this.plugin = plugin;
        determineLayer();
        setPosition(OverlayPosition.DYNAMIC);
        setPriority(OverlayPriority.HIGH);
    }

    @Override
    public Dimension render(Graphics2D graphics)
    {
        // Tunnels MiniMap overlay
        if (config.displayOpenTunnels())
        {
            renderTunnelMinimapOverlay(graphics);
        }

        return null;
    }

    /**
     * Render MiniMap overlay for open tunnels
     *
     * @param graphics
     */
    private void renderTunnelMinimapOverlay(Graphics2D graphics)
    {
        for (WorldPoint deSpawnedRock : plugin.getDeSpawnedRocks())
        {

            if (!config.displayOpenTunnels() || client.getLocalPlayer().getWorldLocation().getRegionID() != RegionID.CASTLE_WARS_UNDERGROUND || deSpawnedRock == null)
            {
                return;
            }

            /**
             * Ugly as f*ck but it works.
             *
             * @TODO REWRITE THIS SHIT
             */
            String Text = "Open";
            LocalPoint deSpawnedRocksLocation = LocalPoint.fromWorld(client, deSpawnedRock);
            net.runelite.api.Point deSpawnedRocksMinimapText = Perspective.getCanvasTextMiniMapLocation(client, graphics,
                    deSpawnedRocksLocation, Text);
            graphics.setColor(Color.GREEN);

            // Saradomin tunnel north direction
            if (deSpawnedRock.getX() == 2409 && deSpawnedRock.getY() == 9503)
            {
                if (deSpawnedRocksMinimapText != null)
                {
                    graphics.drawString(Text, deSpawnedRocksMinimapText.getX() + 1, deSpawnedRocksMinimapText.getY() + 1);
                }
                LocalPoint saradominTunnelNorthLocation = LocalPoint.fromWorld(client, 2424, 9493);
                if (saradominTunnelNorthLocation == null)
                {
                    continue;
                }
                net.runelite.api.Point saradominTunnelMinimapTextNorth = Perspective.getCanvasTextMiniMapLocation(client, graphics,
                        saradominTunnelNorthLocation, Text);
                if (saradominTunnelMinimapTextNorth != null)
                {
                    graphics.drawString(Text, saradominTunnelMinimapTextNorth.getX() + 1, saradominTunnelMinimapTextNorth.getY() + 1);
                }}

            // Saradomin tunnel west direction
            if (deSpawnedRock.getX() == 2401 && deSpawnedRock.getY() == 9494)
            {
                if (deSpawnedRocksMinimapText != null)
                {
                    graphics.drawString(Text, deSpawnedRocksMinimapText.getX() + 1, deSpawnedRocksMinimapText.getY() + 1);
                }
                LocalPoint saradominTunnelWestLocation = LocalPoint.fromWorld(client, 2418, 9483);
                if (saradominTunnelWestLocation == null)
                {
                    continue;
                }
                net.runelite.api.Point saradominTunnelMinimapTextWest = Perspective.getCanvasTextMiniMapLocation(client, graphics,
                        saradominTunnelWestLocation, Text);
                if (saradominTunnelMinimapTextWest != null)
                {
                    graphics.drawString(Text, saradominTunnelMinimapTextWest.getX() + 1, saradominTunnelMinimapTextWest.getY() + 1);
                }}

            // Zamorak tunnel south direction
            if (deSpawnedRock.getX() == 2391 && deSpawnedRock.getY() == 9501)
            {
                if (deSpawnedRocksMinimapText != null)
                {
                    graphics.drawString(Text, deSpawnedRocksMinimapText.getX() + 1, deSpawnedRocksMinimapText.getY() + 1);
                }
                LocalPoint zamorakTunnelSouthLocation = LocalPoint.fromWorld(client, 2371, 9516);
                if (zamorakTunnelSouthLocation == null)
                {
                    continue;
                }
                net.runelite.api.Point zamorakTunnelMinimapTextSouth = Perspective.getCanvasTextMiniMapLocation(client, graphics,
                        zamorakTunnelSouthLocation, Text);
                if (zamorakTunnelMinimapTextSouth != null)
                {
                    graphics.drawString(Text, zamorakTunnelMinimapTextSouth.getX() + 1, zamorakTunnelMinimapTextSouth.getY() + 1);
                }}

            // Zamorak tunnel east direction
            if (deSpawnedRock.getX() == 2400 && deSpawnedRock.getY() == 9512)
            {
                if (deSpawnedRocksMinimapText != null)
                {
                    graphics.drawString(Text, deSpawnedRocksMinimapText.getX() + 1, deSpawnedRocksMinimapText.getY() + 1);
                }
                LocalPoint zamorakTunnelEastLocation = LocalPoint.fromWorld(client, 2382, 9527);
                if (zamorakTunnelEastLocation == null)
                {
                    continue;
                }
                net.runelite.api.Point zamorakTunnelMinimapTextEast = Perspective.getCanvasTextMiniMapLocation(client, graphics,
                        zamorakTunnelEastLocation, Text);
                if (zamorakTunnelMinimapTextEast != null)
                {
                    graphics.drawString(Text, zamorakTunnelMinimapTextEast.getX() + 1, zamorakTunnelMinimapTextEast.getY() + 1);
                }}
        }
    }

    public void determineLayer()
    {
        if (config.mirrorMode())
        {
            setLayer(OverlayLayer.AFTER_MIRROR);
        }
        if (!config.mirrorMode())
        {
            setLayer(OverlayLayer.ABOVE_WIDGETS);
        }
    }
}