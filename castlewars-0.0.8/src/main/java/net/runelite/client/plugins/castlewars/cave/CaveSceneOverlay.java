package net.runelite.client.plugins.castlewars.cave;

import net.runelite.api.Client;
import net.runelite.api.Perspective;
import net.runelite.api.Point;
import net.runelite.api.coords.LocalPoint;
import net.runelite.api.coords.WorldPoint;
import net.runelite.client.plugins.castlewars.CastleWarsConfig;
import net.runelite.client.plugins.castlewars.CastleWarsPlugin;
import net.runelite.client.plugins.castlewars.id.ObjectID;
import net.runelite.client.ui.overlay.Overlay;
import net.runelite.client.ui.overlay.OverlayLayer;
import net.runelite.client.ui.overlay.OverlayPosition;
import net.runelite.client.ui.overlay.OverlayPriority;
import net.runelite.client.ui.overlay.components.ProgressPieComponent;

import javax.inject.Inject;
import java.awt.*;
import java.util.Map;

public class CaveSceneOverlay extends Overlay
{

    private final Client client;
    private final CastleWarsConfig config;
    private final CastleWarsPlugin plugin;

    @Inject
    CaveSceneOverlay(Client client, CastleWarsConfig config, CastleWarsPlugin plugin)
    {
        this.client = client;
        this.config = config;
        this.plugin = plugin;
        determineLayer();
        setPosition(OverlayPosition.DYNAMIC);
        setPriority(OverlayPriority.HIGH);
    }

    @Override
    public Dimension render(Graphics2D graphics)
    {
        // Caves to highlight
        for (Map.Entry<WorldPoint, Cave> entry : plugin.getHighlightCaves().entrySet())
        {
            Cave cave = entry.getValue();

            switch (cave.getCaveID())
            {
                case ObjectID.CAVE:
                    if (config.caveHighlightAsCircle())
                    {
                        renderCircleOnCave(graphics, cave, config.getCaveHighlightColor(), Color.gray);
                    }
                    else
                    {
                        renderCaveSceneOverlay(graphics, cave, config.getCaveHighlightColor());
                    }
                    break;
            }
        }
        return null;
    }

    /**
     * Draws a circle on the cave.
     *
     * @param graphics
     * @param cave The barricade on which the circle needs to be drawn
     * @param fill The fill color of the timer
     * @param border The border color of the timer
     */
    private void renderCircleOnCave(Graphics2D graphics, Cave cave, Color fill, Color border)
    {
        if (cave.getWorldLocation().getPlane() != client.getPlane())
        {
            return;
        }
        LocalPoint localLoc = LocalPoint.fromWorld(client, cave.getWorldLocation());
        if (localLoc == null)
        {
            return;
        }
        net.runelite.api.Point loc = Perspective.localToCanvas(client, localLoc, client.getPlane());

        ProgressPieComponent pie = new ProgressPieComponent();
        pie.setFill(fill);
        pie.setBorderColor(border);
        pie.setPosition(loc);
        pie.setProgress(1);
        pie.render(graphics);
    }

    /**
     * Render Cave highlights
     *
     * @param graphics
     * @param cave
     * @param color
     */
    private void renderCaveSceneOverlay(Graphics2D graphics, Cave cave, Color color)
    {
        if (cave.getWorldLocation().getPlane() != client.getPlane())
        {
            return;
        }
        LocalPoint localLoc = LocalPoint.fromWorld(client, cave.getWorldLocation());
        if (localLoc == null)
        {
            return;
        }
        net.runelite.api.Point loc = Perspective.localToCanvas(client, localLoc, client.getPlane());

        if (loc == null)
        {
            return;
        }

        Point mousePosition = client.getMouseCanvasPosition();
        Shape objectClickbox = cave.getCave().getConvexHull();
        if (objectClickbox == null)
        {
            return;
        }

        if (objectClickbox.contains(mousePosition.getX(), mousePosition.getY()))
        {
            renderPoly(graphics, color.darker(), objectClickbox);
        }
        else
        {
            renderPoly(graphics, color, objectClickbox);
        }

    }

    /**
     * Render Polygon
     *
     * @param graphics
     * @param color
     * @param polygon
     */
    private void renderPoly(Graphics2D graphics, Color color, Shape polygon)
    {
        if (polygon != null)
        {
            graphics.setColor(color);
            graphics.setStroke(new BasicStroke(2));
            graphics.draw(polygon);
            graphics.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), 20));
            graphics.fill(polygon);
        }
    }

    public void determineLayer()
    {
        if (config.mirrorMode())
        {
            setLayer(OverlayLayer.AFTER_MIRROR);
        }
        if (!config.mirrorMode())
        {
            setLayer(OverlayLayer.ABOVE_SCENE);
        }
    }
}