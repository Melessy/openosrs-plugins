package net.runelite.client.plugins.castlewars.cave;

import lombok.Getter;
import net.runelite.api.WallObject;
import net.runelite.api.coords.WorldPoint;

/**
 * Wrapper class for a WallObjects that represents a cave.
 */
public class Cave
{
    /**
     * The WallObject Cave
     */
    @Getter
    private WallObject cave;

    /**
     * The cave id
     */
    @Getter
    private int caveID;

    /**
     *  The worldLocation
     */
    @Getter
    private WorldPoint worldLocation;

    /**
     * Constructor for a Cave WallObject
     *
     * @param cave
     */
    public Cave(WallObject cave)
    {
        this.cave = cave;
        this.caveID = cave.getId();
        this.worldLocation = cave.getWorldLocation();
    }

}


