/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/>
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.runelite.client.plugins.castlewars.tap;

import net.runelite.api.Client;
import net.runelite.api.Perspective;
import net.runelite.api.Point;
import net.runelite.api.coords.LocalPoint;
import net.runelite.api.coords.WorldPoint;
import net.runelite.client.plugins.castlewars.CastleWarsConfig;
import net.runelite.client.plugins.castlewars.CastleWarsPlugin;
import net.runelite.client.plugins.castlewars.id.ObjectID;
import net.runelite.client.ui.overlay.Overlay;
import net.runelite.client.ui.overlay.OverlayLayer;
import net.runelite.client.ui.overlay.OverlayPosition;
import net.runelite.client.ui.overlay.OverlayPriority;

import javax.inject.Inject;
import java.awt.*;
import java.util.Map;

public class TapSceneOverlay extends Overlay
{

    private final Client client;
    private final CastleWarsConfig config;
    private final CastleWarsPlugin plugin;

    @Inject
    TapSceneOverlay(Client client, CastleWarsConfig config, CastleWarsPlugin plugin)
    {
        this.client = client;
        this.config = config;
        this.plugin = plugin;
        determineLayer();
        setPosition(OverlayPosition.DYNAMIC);
        setPriority(OverlayPriority.HIGH);
    }

    @Override
    public Dimension render(Graphics2D graphics)
    {
        /**
         * Taps to highlight
         */
        for (Map.Entry<WorldPoint, Tap> entry : plugin.getHighlightTaps().entrySet())
        {
            Tap tap = entry.getValue();

            switch (tap.getTapID())
            {
                case ObjectID.TAP:
                renderTapSceneOverlay(graphics, tap, config.getTapHighlightColor()); // new Color(-5635841): == magenta
                break;
            }
        }
        return null;
    }

    /**
     * Render Tap highlights
     * @param graphics
     * @param tap
     * @param color
     */
    private void renderTapSceneOverlay(Graphics2D graphics, Tap tap, Color color)
    {
        if (tap.getWorldLocation().getPlane() != client.getPlane())
        {
            return;
        }
        LocalPoint localLoc = LocalPoint.fromWorld(client, tap.getWorldLocation());
        if (localLoc == null)
        {
            return;
        }
        net.runelite.api.Point loc = Perspective.localToCanvas(client, localLoc, client.getPlane());

        if (loc == null)
        {
            return;
        }

        Point mousePosition = client.getMouseCanvasPosition();
        Shape objectClickbox = tap.getTap().getConvexHull();
        if (objectClickbox == null)
        {
            return;
        }

        if (objectClickbox.contains(mousePosition.getX(), mousePosition.getY()))
        {
            renderPoly(graphics, color.darker(), objectClickbox);
        }
        else
        {
            renderPoly(graphics, color, objectClickbox);
        }

    }

    /**
     * Render Polygon
     * @param graphics
     * @param color
     * @param polygon
     */
    private void renderPoly(Graphics2D graphics, Color color, Shape polygon)
    {
        if (polygon != null)
        {
            graphics.setColor(color);
            graphics.setStroke(new BasicStroke(2));
            graphics.draw(polygon);
            graphics.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), 20));
            graphics.fill(polygon);
        }
    }

    public void determineLayer()
    {
        if (config.mirrorMode())
        {
            setLayer(OverlayLayer.AFTER_MIRROR);
        }
        if (!config.mirrorMode())
        {
            setLayer(OverlayLayer.ABOVE_SCENE);
        }
    }
}

