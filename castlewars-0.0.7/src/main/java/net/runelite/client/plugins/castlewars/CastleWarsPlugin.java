/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/>
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.runelite.client.plugins.castlewars;

import com.google.inject.Provides;
import java.time.Instant;
import java.util.*;
import javax.inject.Inject;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.runelite.api.*;
import net.runelite.api.coords.LocalPoint;
import net.runelite.api.coords.WorldPoint;
import net.runelite.api.events.*;
import net.runelite.api.util.Text;
import net.runelite.client.config.ConfigManager;
import net.runelite.client.eventbus.Subscribe;
import net.runelite.client.events.ConfigChanged;
import net.runelite.client.game.ItemManager;
import net.runelite.client.plugins.Plugin;
import net.runelite.client.plugins.PluginDescriptor;
import net.runelite.client.plugins.PluginType;
import net.runelite.client.plugins.castlewars.barricade.Barricade;
import net.runelite.client.plugins.castlewars.barricade.BarricadeMinimapOverlay;
import net.runelite.client.plugins.castlewars.barricade.BarricadeSceneOverlay;
import net.runelite.client.plugins.castlewars.barricade.BarricadeTimerOverlay;
import net.runelite.client.plugins.castlewars.cave.Cave;
import net.runelite.client.plugins.castlewars.cave.CaveSceneOverlay;
import net.runelite.client.plugins.castlewars.door.DoorSceneOverlay;
import net.runelite.client.plugins.castlewars.grounditem.GroundItem;
import net.runelite.client.plugins.castlewars.grounditem.GroundItemOverlay;
import net.runelite.client.plugins.castlewars.id.*;
import net.runelite.client.plugins.castlewars.id.NpcID;
import net.runelite.client.plugins.castlewars.id.ObjectID;
import net.runelite.client.plugins.castlewars.door.Door;
import net.runelite.client.plugins.castlewars.rock.Rock;
import net.runelite.client.plugins.castlewars.rock.RockMinimapOverlay;
import net.runelite.client.plugins.castlewars.rock.RockSceneOverlay;
import net.runelite.client.plugins.castlewars.rock.TunnelMinimapOverlay;
import net.runelite.client.plugins.castlewars.tap.Tap;
import net.runelite.client.plugins.castlewars.tap.TapSceneOverlay;
import net.runelite.client.ui.overlay.OverlayManager;
import org.pf4j.Extension;

@Extension
@PluginDescriptor(
	name = "Castle Wars",
	description = "Castle Wars Plugin",
	tags = {"castlewars", "minigame"},
    type = PluginType.MINIGAME
)

@Slf4j
public class  CastleWarsPlugin extends Plugin
{
    /**
     * The client
     */
    @Inject
    private Client client;

    /**
     * The config
     */
    @Inject
    private CastleWarsConfig config;

    /**
     * The overlay manager
     */
    @Inject
    private OverlayManager overlayManager;

    /**
     * Barricade scene overlay for highlights
     */
    @Inject
    private BarricadeSceneOverlay BarricadeSceneOverlay;

    /**
     * Barricade MiniMap overlay
     */
    @Inject
    private BarricadeMinimapOverlay BarricadeMinimapOverlay;

    /**
     * Barricade timer overlay for lit barricades
     */
    @Inject
    private BarricadeTimerOverlay BarricadeTimerOverlay;

    /**
     * Rocks underground scene overlay for highlights
     */
    @Inject
    private RockSceneOverlay RockSceneOverlay;

    /**
     * Rocks underground MiniMap overlay
     */
    @Inject
    private RockMinimapOverlay RockMinimapOverlay;

    /**
     * Open Tunnel Minimap overlay
     */
    @Inject
    private TunnelMinimapOverlay TunnelMinimapOverlay;

    /**
     * Door scene overlay for highlights
     */
    @Inject
    private DoorSceneOverlay DoorSceneOverlay;

    /**
     * Tap scene overlay for highlights
     */
    @Inject
    private TapSceneOverlay TapSceneOverlay;

    /**
     * Castle Wars game time overlay for resized modes
     */
    @Inject
    private GameTimeOverlay GameTimeOverlay;

    /**
     * GroundItem overlay for tinderbox/bucket
     */
    @Inject
    private GroundItemOverlay GroundItemOverlay;

    /**
     * Underground cave scene overlay for highlights
     */
    @Inject
    private CaveSceneOverlay CaveSceneOverlay;

    /**
     * ItemManager for ground items
     */
    @Inject
    private ItemManager itemManager;

    /**
     * Saradomin standard(flag)
     */
    @Getter(AccessLevel.PACKAGE)
    private GameObject saradominStandard;

    /**
     * Zamorak standard(flag)
     */
    @Getter(AccessLevel.PACKAGE)
    private GameObject zamorakStandard;

    /**
     * DeSpawned rocks underground
     */
    @Getter
    private final List<WorldPoint> deSpawnedRocks = new ArrayList<>();

    /**
     * SpawnedRocks underground to highlight
     */
    @Getter
    private final Map<WorldPoint, Rock> highlightRocks = new HashMap<>();

    /**
     * Set TindTimer on lit barricades
     */
    @Getter
    private final Map<WorldPoint, Barricade> litBarricades = new HashMap<>();

    /**
     * Last action time for TindTimer
     */
    @Getter(AccessLevel.PACKAGE)
    private Instant lastActionTime = Instant.ofEpochMilli(0);

    /**
     * The barricades to highlight for each team blue/red
     */
    @Getter
    private final Set<Barricade> highlightBarricades = new HashSet<>();

    /**
     * The doors to highlight
     */
    @Getter
    private final Map<WorldPoint, Door> highlightDoors = new HashMap<>();

    /**
     * The taps to highlight
     */
    @Getter
    private final Map<WorldPoint, Tap> highlightTaps = new HashMap<>();

    /**
     * The groundItems to highlight, tinderbox/bucket
     */
    @Getter
    private final Map<WorldPoint, GroundItem> highlightGroundItems = new HashMap<>();

    /**
     * The underground caves to highlight
     */
    @Getter
    private final Map<WorldPoint, Cave> highlightCaves = new HashMap<>();

    /**
     * @param configManager
     * @return CastleWarsConfig
     */
    @Provides
    CastleWarsConfig getConfig(ConfigManager configManager)
    {
        return configManager.getConfig(CastleWarsConfig.class);
    }

    @SneakyThrows
    @Subscribe
    public void onConfigChanged(ConfigChanged event)
    {
        if (!event.getGroup().equals("castlewars"))
        {
            return;
        }

        if (event.getKey().equals("mirrorMode"))
        {
            BarricadeSceneOverlay.determineLayer();
            BarricadeMinimapOverlay.determineLayer();
            BarricadeTimerOverlay.determineLayer();

            RockSceneOverlay.determineLayer();
            RockMinimapOverlay.determineLayer();
            TunnelMinimapOverlay.determineLayer();

            DoorSceneOverlay.determineLayer();
            TapSceneOverlay.determineLayer();

            GameTimeOverlay.determineLayer();

            CaveSceneOverlay.determineLayer();

            overlayManager.remove(BarricadeSceneOverlay);
            overlayManager.remove(BarricadeMinimapOverlay);
            overlayManager.remove(BarricadeTimerOverlay);

            overlayManager.remove(RockSceneOverlay);
            overlayManager.remove(RockMinimapOverlay);
            overlayManager.remove(TunnelMinimapOverlay);

            overlayManager.remove(DoorSceneOverlay);
            overlayManager.remove(TapSceneOverlay);

            overlayManager.remove(GameTimeOverlay);

            overlayManager.remove(CaveSceneOverlay);

            overlayManager.add(BarricadeSceneOverlay);
            overlayManager.add(BarricadeMinimapOverlay);
            overlayManager.add(BarricadeTimerOverlay);

            overlayManager.add(RockSceneOverlay);
            overlayManager.add(RockMinimapOverlay);
            overlayManager.add(TunnelMinimapOverlay);

            overlayManager.add(DoorSceneOverlay);
            overlayManager.add(TapSceneOverlay);

            overlayManager.add(GameTimeOverlay);

            overlayManager.add(CaveSceneOverlay);
        }

        if (!config.barricadeHighlight())
        {
            highlightBarricades.clear();
        }
        if (!config.rocksHighlight())
        {
            highlightRocks.clear();
        }
        if (!config.useTindTimer())
        {
            litBarricades.clear();
        }
       if (!config.doorsHighlight())
       {
           highlightDoors.clear();
       }
        if (!config.tapHighlight())
        {
            highlightTaps.clear();
        }
        if (!config.groundItemHighlight())
        {
            highlightGroundItems.clear();
        }
        if (!config.caveHighlight())
        {
            highlightCaves.clear();
        }
        rebuildAllHighlightBarricades();
        BarricadeTimerOverlay.updateConfig();
    }

    /**
     * Rebuild highlight barricades when config has changed.
     */
    private void rebuildAllHighlightBarricades()
    {
        highlightBarricades.clear();

        if (client.getGameState() != GameState.LOGGED_IN)
        {
            return;
        }

        for (NPC npc : client.getNpcs())
        {
            //final WorldPoint npcLocation = npc.getWorldLocation();
            switch (npc.getId())
            {
                case NpcID.SARADOMIN_BARRICADE:
                case NpcID.ZAMORAK_BARRICADE:
                    if (config.barricadeHighlight())
                    {
                        highlightBarricades.add(new Barricade(npc));
                    }
                    break;

            }
        }
    }

    @SneakyThrows
    @Override
    protected void startUp()
    {
        overlayManager.add(BarricadeSceneOverlay);
        overlayManager.add(BarricadeMinimapOverlay);
        overlayManager.add(BarricadeTimerOverlay);

        overlayManager.add(RockSceneOverlay);
        overlayManager.add(RockMinimapOverlay);
        overlayManager.add(TunnelMinimapOverlay);

        overlayManager.add(DoorSceneOverlay);
        overlayManager.add(TapSceneOverlay);

        overlayManager.add(GameTimeOverlay);
        BarricadeTimerOverlay.updateConfig();

        overlayManager.add(GroundItemOverlay);

        overlayManager.add(CaveSceneOverlay);
    }

    @SneakyThrows
    @Override
    protected void shutDown()
    {
        overlayManager.remove(BarricadeSceneOverlay);
        overlayManager.remove(BarricadeMinimapOverlay);
        overlayManager.remove(BarricadeTimerOverlay);

        overlayManager.remove(RockSceneOverlay);
        overlayManager.remove(RockMinimapOverlay);
        overlayManager.remove(TunnelMinimapOverlay);

        overlayManager.remove(DoorSceneOverlay);
        overlayManager.remove(TapSceneOverlay);

        overlayManager.remove(GameTimeOverlay);
        lastActionTime = Instant.ofEpochMilli(0);
        litBarricades.clear();
        highlightBarricades.clear();
        highlightRocks.clear();
        deSpawnedRocks.clear();
        overlayManager.remove(GroundItemOverlay);
        overlayManager.remove(CaveSceneOverlay);
    }

    @Subscribe
    public void onGameStateChanged(final GameStateChanged event)
    {
        /**
         * Clear all things when logged out.
         */
        if (event.getGameState() == GameState.HOPPING || event.getGameState() == GameState.LOGIN_SCREEN)
        {
            deSpawnedRocks.clear();
            litBarricades.clear();
            highlightBarricades.clear();
            highlightRocks.clear();
            saradominStandard = null;
            zamorakStandard = null;
        }
    }

    /**
     * Method to check if inside Castle Wars.
     */
    private boolean inCastleWars()
    {
        Player localPlayer = client.getLocalPlayer();
        return localPlayer != null && localPlayer.getWorldLocation().getRegionID() == RegionID.CASTLE_WARS  // 9520 = Castle Wars
                || localPlayer != null &&localPlayer.getWorldLocation().getRegionID() == RegionID.CASTLE_WARS_UNDERGROUND; // 9620 = Castle Wars underground
    }

    @Subscribe
    public void onMenuEntryAdded(MenuEntryAdded menuEntryAdded)
    {
        String option = Text.removeTags(menuEntryAdded.getOption()).toLowerCase();
        String target = Text.standardize(menuEntryAdded.getTarget());
        int identifier = menuEntryAdded.getIdentifier();
        MenuEntry[] menuEntries = client.getMenuEntries();
        Player[] players = client.getCachedPlayers();
        NPC[] npcs = client.getCachedNPCs();

        /**
         * Don't hide player options when not inside Castle Wars, or when using bandages to heal other players.
         */
        if (!inCastleWars() || target.contains("bandages"))
            return;

        /**
         * Hide player options when using explosion, tinderbox or bucket.
         */
        if (config.hidePlayerOptions() && option.startsWith("use"))
        {
            if (target.contains("explosive potion")
                    || target.contains("tinderbox")
                    || target.contains("bucket of water")
                    || target.contains("bucket"))
            {
             Player player = null;

            if (identifier >= 0 && identifier < players.length)
                player = players[identifier];

            if (player == null)
                return;

            if (menuEntries.length > 0 && target.contains(player.getName().toLowerCase()))
                client.setMenuEntries(Arrays.copyOf(menuEntries, menuEntries.length - 1));
        }}

        /**
         * Hide barricade(NPC) options when standard(flag, GameObject) is on same location as the barricade(NPC).
         */
        if (config.hideNpcOptions())
        {
            if (saradominStandard == null && zamorakStandard == null)
                return;

            NPC npc = null;

            if (identifier >= 0 && identifier < npcs.length)
                npc = npcs[identifier];

           if (npc == null)
               return;

            if (saradominStandard != null && saradominStandard.getLocalLocation().equals(npc.getLocalLocation()) || zamorakStandard != null && zamorakStandard.getLocalLocation().equals(npc.getLocalLocation()))
                if (menuEntries.length > 0 && target.contains(npc.getName().toLowerCase()))
                    client.setMenuEntries(Arrays.copyOf(menuEntries, menuEntries.length -1));
        }
    }

    @Subscribe
    public void onGameObjectSpawned(GameObjectSpawned event)
    {
        final GameObject gameObject = event.getGameObject();
        final WorldPoint gameObjectLocation = gameObject.getWorldLocation();

        final Tile tile = event.getTile();
        final WorldPoint tileLocation = tile.getWorldLocation();

        switch (gameObject.getId())
        {
            /**
             * This is used to know which standard(flag) is dropped by a player.
             */
            case ObjectID.SARADOMIN_STANDARD: // Saradomin Standard
                this.saradominStandard = gameObject;
                log.debug("Saradomin flag spawn: {}", gameObject);
                break;

            case ObjectID.ZAMORAK_STANDARD: // Zamorak Standard
                this.zamorakStandard = gameObject;
                log.debug("Zamorak flag spawn: {}", gameObject);
                break;

            /**
             * Remove tunnel MiniMap overlay if rock has spawned and add highlights on rocks.
             */
            case ObjectID.ROCKS_FULL: // Underground rocks full
            case ObjectID.ROCKS_HALF: // Underground rocks half
                deSpawnedRocks.remove(tileLocation);
                if (config.rocksHighlight() && gameObject.getWorldLocation().getRegionID() == RegionID.CASTLE_WARS_UNDERGROUND)
                {
                    highlightRocks.put(gameObjectLocation, new Rock(gameObject));
                }
                log.debug("Rock spawn: {}", gameObject);
                break;
        }
    }

    @Subscribe
    public void onGameObjectDespawned(GameObjectDespawned event)
    {
        final GameObject gameObject = event.getGameObject();
        final WorldPoint gameObjectLocation = gameObject.getWorldLocation();

        final Tile tile = event.getTile();
        final WorldPoint tileLocation = tile.getWorldLocation();

        switch (gameObject.getId())
        {
            /**
             * null standards if it has been taken again.
             */
            case ObjectID.SARADOMIN_STANDARD: // Saradomin Standard
                this.saradominStandard = null;
                break;

            case ObjectID.ZAMORAK_STANDARD: // Zamorak Standard
                this.zamorakStandard = null;
                break;

            /**
             * Remove highlight and add tunnel MiniMap overlay if rock has been deSpawned.
             */
            case ObjectID.ROCKS_FULL: // Underground rocks Full
            case ObjectID.ROCKS_HALF: // Underground rocks half
                highlightRocks.remove(gameObjectLocation);
                deSpawnedRocks.add(tileLocation);
                break;
        }
    }

    @Subscribe
    public void onGameTick(GameTick tick)
    {
        /**
         * Set hintArrow on player with the standard(flag).
         */
        for (Player player : client.getPlayers())
        {
            int[] equipmentIds = player.getPlayerAppearance().getEquipmentIds();
            for (int equipmentId : equipmentIds)
            {
                if (equipmentId == EquipmentID.SARADOMIN_STANDARD
                        || equipmentId == EquipmentID.ZAMORAK_STANDARD)
                    client.setHintArrow(player);
            }
        }
              /*if (!inCastleWars())
              {
                  if (!highlightBarricades.isEmpty())
                  {
                      highlightBarricades.clear();
                  }
                  if (!highlightRocks.isEmpty())
                  {
                      highlightRocks.clear();
                  }
                  if (!highlightDoors.isEmpty())
                  {
                      highlightDoors.clear();
                  }
                  if (!highlightTaps.isEmpty())
                  {
                      highlightTaps.clear();
                  }
                  if (!highlightGroundItems.isEmpty())
                  {
                      highlightGroundItems.clear();
                  }
              }*/

        /**
         * Clear highlights from rocks if player is not underground.
         */
        if (client.getLocalPlayer().getWorldLocation().getRegionID() != RegionID.CASTLE_WARS_UNDERGROUND && !highlightRocks.isEmpty())
        {
            highlightRocks.clear();
        }

        /**
         * Clear highlights from doors if not inside CastleWars or when underground.
         */
        if (!inCastleWars()
                && !highlightDoors.isEmpty()
                || !highlightDoors.isEmpty() && client.getLocalPlayer().getWorldLocation().getRegionID() == RegionID.CASTLE_WARS_UNDERGROUND)
        {
            highlightDoors.clear();
        }

        /**
         * Check if all TindTimers are still there, and remove the ones that are not.
         */
        Iterator<Map.Entry<WorldPoint, Barricade>> it = getLitBarricades().entrySet().iterator();
        Tile[][][] tiles = client.getScene().getTiles();

        //Instant expire = Instant.now().minus(Barricade.TIND_TIME.multipliedBy(2));
        Instant expire = Instant.now().plusSeconds(7);

        while (it.hasNext())
        {
            Map.Entry<WorldPoint, Barricade> entry = it.next();
            WorldPoint worldPoint = entry.getKey();
            Barricade barricade = entry.getValue();
            LocalPoint localPoint = LocalPoint.fromWorld(client, worldPoint);

            // Not within the client's viewport
            if (localPoint == null)
            {
                // remove TindTimer if it has expired
                if (barricade.getLitOn().isBefore(expire))
                {
                    it.remove();
                }
            }
        }

        /**
         * Check if all highlighted Rocks are still there, and remove the ones that are not.
         */
        Iterator<Map.Entry<WorldPoint, Rock>> highlightRocksIter = getHighlightRocks().entrySet().iterator();
        while (highlightRocksIter.hasNext())
        {
            Map.Entry<WorldPoint, Rock> entry = highlightRocksIter.next();
            WorldPoint worldPoint = entry.getKey();
            Rock rock = entry.getValue();
            LocalPoint localPoint = LocalPoint.fromWorld(client, worldPoint);

            // Not within the client's viewport
            if (localPoint == null)
            {
                highlightRocksIter.remove();
            }
        }

        /**
         * Check if all highlighted Doors are still there, and remove the ones that are not.
         */
        Iterator<Map.Entry<WorldPoint, Door>> highlightDoorsIter = getHighlightDoors().entrySet().iterator();
        while (highlightDoorsIter.hasNext())
        {
            Map.Entry<WorldPoint, Door> entry = highlightDoorsIter.next();
            WorldPoint worldPoint = entry.getKey();
            Door door = entry.getValue();
            LocalPoint localPoint = LocalPoint.fromWorld(client, worldPoint);

            // Not within the client's viewport
            if (localPoint == null)
            {
                highlightDoorsIter.remove();
            }
        }
    }

    @Subscribe
    public void onNpcSpawned(NpcSpawned npcSpawned)
    {
        /**
         * Only allow this to work when barricadeHighlight is enabled in config.
         */
        if (!config.barricadeHighlight())
            return;

        NPC npc = npcSpawned.getNpc();
        //final WorldPoint npcLocation = npc.getWorldLocation();

        switch (npc.getId())
        {
            /**
             * Set highlights on barricades. ->
             * Normal barricade,
             * Lit barricade
             */
            case NpcID.SARADOMIN_BARRICADE:
            case NpcID.SARADOMIN_BARRICADE_LIT:
                highlightBarricades.add(new Barricade(npc));
                log.debug("Saradomin barricade spawn: {}", npc);
                break;

            case NpcID.ZAMORAK_BARRICADE:
            case NpcID.ZAMORAK_BARRICADE_LIT:
                highlightBarricades.add(new Barricade(npc));
                log.debug("Zamorak barricade spawn: {}", npc);
                break;
        }}

    @Subscribe
    public void onNpcDefinitionChanged(NpcDefinitionChanged npcDefinitionChanged)
    {
        NPC npc = npcDefinitionChanged.getNpc();
        final WorldPoint npcLocation = npc.getWorldLocation();

        switch (npc.getId())
        {
            /**
             * Remove TindTimers from lit barricades when bucket is used.
             */
            case NpcID.SARADOMIN_BARRICADE_LIT:
            case NpcID.ZAMORAK_BARRICADE_LIT:
                litBarricades.remove(npcLocation);
                break;

            /**
             * Set highlights and TindTimers on lit barricades.
             */
            case NpcID.SARADOMIN_BARRICADE:
            case NpcID.ZAMORAK_BARRICADE:
                if (config.useTindTimer())
                {
                    litBarricades.put(npcLocation, new Barricade(npc));
                    lastActionTime = Instant.now();
                }
              break;
        }}

   @Subscribe
	public void onNpcDespawned(NpcDespawned npcDespawned)
	{
        /**
         * Remove highlights and TindTimers from lit barricades.
         */
            NPC npc = npcDespawned.getNpc();
            final WorldPoint npcLocation = npc.getWorldLocation();
            //final Barricade litcade = barricades.get(npcLocation);

            switch (npc.getId())
            {
                           case NpcID.SARADOMIN_BARRICADE:
                           case NpcID.ZAMORAK_BARRICADE:
                           highlightBarricades.remove(npc);
                           break;

                           case NpcID.SARADOMIN_BARRICADE_LIT:
                           case NpcID.ZAMORAK_BARRICADE_LIT:
                           highlightBarricades.remove(npc);
                           litBarricades.remove(npcLocation);
                           break;
        }}

    @Subscribe
    public void onWallObjectSpawned(WallObjectSpawned event)
    {
        WallObject wallObject = event.getWallObject();
        //final ObjectDefinition comp = client.getObjectDefinition(wallObject.getId());
        //final ObjectDefinition impostor = comp.getImpostorIds() != null ? comp.getImpostor() : comp;
        final WorldPoint wallObjectLocation = wallObject.getWorldLocation();

        if (wallObject != null && ObjectID.wallObject_Ids_DOORS.contains(wallObject.getId()))
        {
            highlightDoors.put(wallObjectLocation, new Door(wallObject));
        }

            if (wallObject != null && wallObject.getId() == ObjectID.CAVE)
            {
                highlightCaves.put(wallObjectLocation, new Cave(wallObject));
            }
    }

    @Subscribe
    public void onWallObjectDespawned(WallObjectDespawned event)
    {
        WallObject wallObject = event.getWallObject();
        final WorldPoint wallObjectLocation = wallObject.getWorldLocation();

        if (ObjectID.wallObject_Ids_DOORS.contains(wallObject.getId()))
        {
            highlightDoors.remove(wallObjectLocation);
        }

        if (wallObject.getId() == ObjectID.CAVE)
        {
            highlightCaves.remove(wallObjectLocation);
        }
    }

    @Subscribe
    public void onDecorativeObjectSpawned(DecorativeObjectSpawned event)
    {
        DecorativeObject decorativeObject = event.getDecorativeObject();
        final WorldPoint decorativeObjectLocation = decorativeObject.getWorldLocation();

        switch (decorativeObject.getId())
        {
            case ObjectID.TAP:
                if (decorativeObject.getWorldLocation().equals(new WorldPoint(2431, 3077, 0))
                || decorativeObject.getWorldLocation().equals(new WorldPoint(2368, 3130, 0)))
                {
                    highlightTaps.put(decorativeObjectLocation, new Tap(decorativeObject));
                }
            break;
        }
    }

    @Subscribe
    public void onDecorativeObjectDespawned(DecorativeObjectDespawned event)
    {
        DecorativeObject decorativeObject = event.getDecorativeObject();
        final WorldPoint decorativeObjectLocation = decorativeObject.getWorldLocation();

        switch (decorativeObject.getId())
        {
            case ObjectID.TAP:
                highlightTaps.remove(decorativeObjectLocation);
                break;
        }
    }

    @Subscribe
    public void onItemSpawned(ItemSpawned event)
    {
        TileItem item = event.getItem();
        Tile tile = item.getTile();

        switch (item.getId())
        {
            case GroundItemID.TINDERBOX:
            case GroundItemID.BUCKET:
                GroundItem groundItem = buildGroundItem(tile, item);
                //highlightGroundItems.putIfAbsent(groundItemKey, groundItem);
                //highlightGroundItems.put(tile.getWorldLocation(), groundItem);
                highlightGroundItems.putIfAbsent(tile.getWorldLocation(), groundItem);
            break;
        }
    }

    @Subscribe
    public void onItemDespawned(ItemDespawned event)
    {
        TileItem item = event.getItem();
        Tile tile = item.getTile();
        //GroundItem.GroundItemKey groundItemKey = new GroundItem.GroundItemKey(item.getId(), tile.getWorldLocation());
        switch (item.getId())
        {
            case GroundItemID.TINDERBOX:
            case GroundItemID.BUCKET:
                if (highlightGroundItems.containsKey(tile.getWorldLocation()))
                {
                    highlightGroundItems.remove(tile.getWorldLocation());
                }
            break;
        }
    }

    private GroundItem buildGroundItem(final Tile tile, final TileItem item)
    {
        // Collect the data for the item
        final int itemId = item.getId();
        final ItemDefinition itemComposition = itemManager.getItemDefinition(itemId);
        final GroundItem groundItem = GroundItem.builder()
                .id(itemId)
                .location(tile.getWorldLocation())
                .height(tile.getItemLayer().getHeight())
                .name(itemComposition.getName())
                .build();
                return groundItem;
    }
}
