package net.runelite.client.plugins.castlewars;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import net.runelite.api.Client;
import net.runelite.api.Point;
import net.runelite.api.widgets.Widget;
import net.runelite.client.ui.overlay.*;

import javax.inject.Inject;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.Thread.sleep;

public class StatsOverlay extends Overlay
{

    private final CastleWarsConfig config;
    private final Client client;

    @Getter(AccessLevel.PACKAGE)
    @Setter(AccessLevel.PACKAGE)
    protected int win = 0;
    @Getter(AccessLevel.PACKAGE)
    @Setter(AccessLevel.PACKAGE)
    protected int lost = 0;
    @Getter(AccessLevel.PACKAGE)
    @Setter(AccessLevel.PACKAGE)
    protected int tie = 0;

    @Inject
    private StatsOverlay(CastleWarsConfig config, Client client)
    {
        this.config = config;
        this.client = client;
        determineLayer();
        setPosition(OverlayPosition.DYNAMIC);
        setPriority(OverlayPriority.HIGH);
    }

    public void writeStatsToFile(String stats)
    {
        final File RuneLiteDir = new File(System.getProperty("user.home") + System.getProperty("file.separator") + ".runelite" + System.getProperty("file.separator") + "castlewars.stats");
        try (OutputStream os = Files.newOutputStream(Paths.get(RuneLiteDir.getAbsolutePath()), StandardOpenOption.CREATE, StandardOpenOption.WRITE,
                StandardOpenOption.APPEND); PrintWriter writer = new PrintWriter(os))
        {
            writer.append(stats); //Append to file
            writer.flush(); // Flush writer
            //writer.close(); // Close writer
        }
        catch (IOException ex)
        {
            Logger.getLogger(StatsOverlay.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Render CastleWars Game Time on resized modes.
     * @param graphics
     * @return null
     */
    @SneakyThrows
    @Override
    public Dimension render(Graphics2D graphics)
    {
            if (!config.displayStats())
            {
                return null;
            }

        Widget saradominTimeWidget = client.getWidget(58, 25); // Castle Wars saradomin time widget
        Widget saradominScoreWidget = client.getWidget(59, 18); // Castle Wars saradomin score widget

        Widget zamorakTimeWidget = client.getWidget(59, 25); // Castle Wars zamorak time widget
        Widget zamorakScoreWidget = client.getWidget(59, 17); // Castle Wars zamorak score widget
        if (saradominScoreWidget != null && zamorakScoreWidget != null)
        {
            final int saradominScore = Integer.parseInt(saradominScoreWidget.getText().replace(" = Saradomin", ""));
            final int zamorakScore = Integer.parseInt(zamorakScoreWidget.getText().replace("Zamorak = ", ""));

        if (saradominTimeWidget != null)
        {
            if (saradominTimeWidget.getText().toLowerCase().equals("0 min") && config.displayStats())
            {
                if (saradominScore > zamorakScore)
                {
                    this.setWin(+1);
                    //writeStatsToFile("win="+getWin());
                    sleep(1000);
                }
                else
                {
                    if (saradominScore == zamorakScore)
                    {
                        this.setTie(+1);
                       //writeStatsToFile("tie="+getTie());
                        sleep(1000);
                    }
                    else
                    {
                        this.setLost(+1);
                        //writeStatsToFile("lost="+getLost());
                        sleep(1000);
                    }
                }
            }
        }
        else
        {
            if (zamorakTimeWidget != null)
            {
                if (zamorakTimeWidget.getText().toLowerCase().equals("0 min"))
                {
                    if (zamorakScore > saradominScore)
                    {
                        this.setWin(+1);
                        //writeStatsToFile("win="+getWin());
                        sleep(1000);
                    }
                    else
                    {
                        if (zamorakScore == saradominScore)
                        {
                            this.setTie(+1);
                            //writeStatsToFile("tie="+getTie());
                            sleep(1000);
                        }
                        else
                        {
                            this.setLost(+1);
                            //writeStatsToFile("lost="+getLost());
                            sleep(1000);
                        }
                    }
                }
            }
        }
        }

            final String text = "win: " + getWin() + " tie: " + getTie() + " lost: " + getLost();
            if (text != null)
            {
                //final net.runelite.api.Point point = new Point(15, 332);
                //OverlayUtil.renderTextLocation(graphics, point, text, Color.yellow);

                OverlayUtil.renderTextLocation(graphics, new Point(10, 308), "win: " + getWin(), Color.yellow);
                OverlayUtil.renderTextLocation(graphics, new Point(10, 320), "tie: " + getTie(), Color.yellow);
                OverlayUtil.renderTextLocation(graphics, new Point(10, 332), "lost: " + getLost(), Color.yellow);
            }
                return null;
            }

    public void determineLayer()
    {
        if (config.mirrorMode())
        {
            setLayer(OverlayLayer.AFTER_MIRROR);
        }
        if (!config.mirrorMode())
        {
            setLayer(OverlayLayer.ABOVE_WIDGETS);
        }
    }
}
