/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/>
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.runelite.client.plugins.castlewars.grounditem;

import com.google.inject.Inject;
import net.runelite.api.Client;
import net.runelite.api.Perspective;
import net.runelite.api.Player;
import net.runelite.api.coords.LocalPoint;
import net.runelite.api.coords.WorldPoint;
import net.runelite.client.plugins.castlewars.CastleWarsConfig;
import net.runelite.client.plugins.castlewars.CastleWarsPlugin;
import net.runelite.client.plugins.castlewars.barricade.Barricade;
import net.runelite.client.plugins.castlewars.door.Door;
import net.runelite.client.ui.overlay.Overlay;
import net.runelite.client.ui.overlay.OverlayLayer;
import net.runelite.client.ui.overlay.OverlayPosition;
import net.runelite.client.ui.overlay.components.TextComponent;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class GroundItemOverlay extends Overlay
{
    private static final int MAX_DISTANCE = 2500;
    // We must offset the text on the z-axis such that
    // it doesn't obscure the ground items below it.
    private static final int OFFSET_Z = 20;
    // The 15 pixel gap between each drawn ground item.
    private static final int STRING_GAP = 15;

    private final Client client;
    private final CastleWarsPlugin plugin;
    private final CastleWarsConfig config;
    private final StringBuilder itemStringBuilder = new StringBuilder();
    private final TextComponent textComponent = new TextComponent();
    private final Map<WorldPoint, Integer> offsetMap = new HashMap<>();

    @Inject
    private GroundItemOverlay(Client client, CastleWarsPlugin plugin, CastleWarsConfig config)
    {
        setPosition(OverlayPosition.DYNAMIC);
        setLayer(OverlayLayer.ABOVE_SCENE);
        this.client = client;
        this.plugin = plugin;
        this.config = config;
    }

    @Override
    public Dimension render(Graphics2D graphics)
    {
        final Player player = client.getLocalPlayer();

        if (!config.groundItemHighlight() || player == null || client.getViewportWidget() == null)
        {
            return null;
        }

        offsetMap.clear();
        final LocalPoint localLocation = player.getLocalLocation();

        for (Map.Entry<WorldPoint, GroundItem> entry : plugin.getHighlightGroundItems().entrySet())
        {
            GroundItem item = entry.getValue();
            final LocalPoint groundPoint = LocalPoint.fromWorld(client, item.getLocation());

            if (groundPoint == null || localLocation.distanceTo(groundPoint) > MAX_DISTANCE) {
                continue;
            }

            itemStringBuilder.append(item.getName());

            final String itemString = itemStringBuilder.toString();
            itemStringBuilder.setLength(0);

            final net.runelite.api.Point textPoint = Perspective.getCanvasTextLocation(client,
                    graphics,
                    groundPoint,
                    itemString,
                    item.getHeight() + OFFSET_Z);

            if (textPoint == null)
            {
                continue;
            }

            final int offset = offsetMap.compute(item.getLocation(), (k, v) -> v != null ? v + 1 : 0);

            final int textX = textPoint.getX();
            final int textY = textPoint.getY() - (STRING_GAP * offset);

                    textComponent.setText(itemString);
                    textComponent.setColor(config.getGroundItemHighlightColor());
                    //textComponent.setOutline(outline);
                    textComponent.setPosition(new java.awt.Point(textX, textY));
                    textComponent.render(graphics);
            }
        return null;
    }
}


